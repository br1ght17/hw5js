// TASK1

function fraction(a,b){
    return a - b
}

console.log(fraction(12,6))

// TASK2

let a = +prompt("Введіть перше число")
let b = +prompt("Введіть друге число")

while(true){
    if(a && b){
        break
    }
    a = prompt("Було введене не вірне значення, введіть перше число")
    b = prompt("Було введене не вірне значення, введіть друге число")
}

let operation = prompt("Яку операцію виконати? (+,-,*,/)")
while(true){
    if(operation === "+" || operation === "-" || operation === "*" || operation === "/"){
        break
    }
    alert("Такої операції не існує")
    operation = prompt("Яку операцію виконати? (+,-,*,/)")
}
function calculator(a,b,operation){
    switch(operation){
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            return (a / b).toFixed(2);
    }
}

console.log(calculator(a,b,operation))

// TASK3

let num;

while(true){
    num = +prompt("Введіть число")
    if(num){
        break
    }
    alert("Було введене не вірне значення")
}
function factorial(x){
    let result = 1
    for(let i = 1; i <= x; i++){
        result *= i
    }
    return result;
}

console.log(factorial(num))